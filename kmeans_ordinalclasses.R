purdist <- function(km, class0) {
  # For each cluster, calculation of the distance between the class with the highest purity
  # and the other classes predominently present in this cluster.
  #
  # Arguments
  # - km : object of class "kmeans" (returned by the fonction kmeans).
  # - class0 : the classes given by the empirical notation.
  #
  # Value
  # - a data frame. Each row corresponds to an empirical class.
  #   Columns:
  #        - class: the class of the empirical notation.
  #        - clustmaj: cluster containing the majority of the elements of the class.
  #        - purity: purity of the class (proportion of its elements which belong
  #                   to the cluster containing the majority of its elements).
  #        - dist: distance between this class the one with the highest purity
  #                   in this cluster.
  
  # Result of the clustering: for each individual, the cluster it is allocated to
  classkm <- as.ordered(km$cluster)

  # Data frame containing the empirical notes (class0) and the clusters (classkm)
  clust <- data.frame(class0 = class0, clust = classkm)
  
  # Initialisation of the data frame containing:
  #        - clustmaj : cluster containing the majority of the elements of the class.
  #        - purity : purity of the class.
  classes <- data.frame(clustmaj = factor(character(nlevels(classkm)), levels = levels(classkm), ordered = FALSE),
                        purity = numeric(nlevels(class0)),
                        row.names = levels(class0))
  
  # For each class:
  for (cl in levels(class0)) {
    # number of elements in class cl in each cluster
    tabcl <- table(clust[clust$class0 == cl, "clust"])
    # cluster containing the highest number of elements in class cl
    classes[cl, "clustmaj"] <- names(tabcl)[which.max(tabcl)]
    # purity of the class cl
    classes[cl, "purity"] <- max(tabcl)/sum(tabcl)
  }

  # Initialisation of the data frame which will be retourned
  clusters <- data.frame(
    class = factor(character(nlevels(class0)), levels = levels(class0), ordered = TRUE),
    clustmaj = factor(character(nlevels(classkm)), levels = levels(classkm), ordered = TRUE),
    purity = numeric(nlevels(class0)),
    dist = numeric(nlevels(class0)),
    row.names = levels(class0))
  
  # For each cluster:
  for (cl in levels(classkm)) {
    # cl: majority classes in the cluster clust
    classclust <- classes[classes$clustmaj == cl, ]
    cl0 <- rownames(classclust)
    # clpmaj: class with the highest purity in cluster clust
    clpmaj <- cl0[which.max(classclust$purity)]
    # distcl: distance between each class predominantly present in clust and the class clpmaj
    distcl <- abs(as.numeric(cl0) - as.numeric(clpmaj))
    classclust <- data.frame(class = cl0, classclust, dist = distcl, stringsAsFactors = FALSE)
    # add the informations calculated for the cluster clust to the data frame clusters
    clusters[cl0, ] <- classclust
  }
  
  return(clusters)
}

kmeansn <- function(x, var = 1:(ncol(x)-1), class = ncol(x), n = 10, scale = TRUE) {
  # The clustering (kmeans) is applied n times. For each segmentation,
  # the fonction clusterdist is applied.
  # Results returned in a data frame clustdist.
  #
  # Arguments
  # - x:      data frame (the data).
  # - var:    integer or character vector.
  #         Numbers or names of the columns containing the variables on which the
  #         clustering is applied.
  # - class:  integer or character. Number or name of the column containing the
  #         empirical notations.
  # - n:      integer. Number of execution of the clustering.
  # - scale:  logical. Are the data scaled before applying the clustering?
  #
  # Value
  # list of 4 elements:
  # - clustdist: data frame. Each row corresponds to a class a priori.
  #   Columns :
  #        - class:  class of the empirical notation.
  #        - columns 2 to 4: results of the 1st clustering:
  #                   - clustmaj: cluster containing the majority of the elements
  #                              of the class.
  #                   - purity: purity of the class.
  #                   - dist: distance between this class and the class with the highest
  #                              purity in this cluster (distance computed in each class).
  #        - Next columns: Idem for each clustering.
  # - purdist.mean: data frame. Each row corresponds to an empirical class.
  #   Columns:
  #        - purity: the mean purity in the class.
  #        - distance: for the considered class, mean distance to the class with the
  #                   highest purity its associated cluster.
  # - classification: data frame. Each row corresponds to an individual (genotype).
  #   Column:
  #        - ind: identifier of the individuals.
  #        - class: the class a priori containing each individual.
  #        - cluster1, dist1: for each individual, the cluster to which it is allocated
  #                   by the 1st clustering, and distance between the cluster and its
  #                   class a priori.
  #                   The clusters are renumbered, so that the cluster to which
  #                   the individual is allocated has the same number as its class
  #                   a priori.
  #        - cluster2, dist2, cluster3...: the same for each clustering.
  # - confusion.matrix : for each clustering, the confusion matrix.
  #   This confusion matrix is built using the clusters and classes a priori
  #   contained by the data frame classification.
  
  # Classes of the empirical (a priori) notation
  class0 <- x[, class]
  if (!is.ordered(class0)) class0 <- ordered(class0)
  
  # The data on which the clustering is applied
  x <- x[var]
  # if (scale) x <- scale(x)
  if (scale) x <- apply(x, 2, function(x) x/max(x))
  View(x)
  
  # Classes a priori (SNES notation)
  kmcluster <- data.frame(ind = rownames(x), class = class0)
  
  # The clustering method is applied n fois.
  # data frame which will contain, for each clustering, the majority class.
  # purity and distance by class
  clustdist <- data.frame(class = levels(class0), row.names = levels(class0))
  CM <- list()
  # kmcluster <- list()
  for (k in 1:n) {
    # application of the clustering (kmeans)
    km <- kmeans(scale(x), centers = 7, nstart = 1)
    kmclust <- km$cluster
    # For each class of the empirical notation, determine the cluster
    # in which it is predominantly present and calculus of its purity;
    # computation, for each cluster, of the distance between the class with the
    # highest purity and the other classes predominant in this cluster
    clust <- purdist(km, class0)
    
    # renumber the levels of clust$clutmaj according to clust$class
    clm <- as.character(clust$clustmaj)
    # n <- 1
    for (cl in unique(clm)) {
      # ncl <- as.character(clust[which(clm == cl), "class"])
      ncl <- which(clm == cl)
      clustn <- clust[ncl, ]
      clm[clm == cl] <- kmclust[kmclust == cl] <- paste0("k", clustn[which.max(clustn$purity), "class"])
    }
    clust$clustmaj <- factor(clm, levels = paste0("k", levels(clustdist$class)))

    kmclust <- factor(kmclust, levels = paste0("k", levels(clustdist$class)), ordered = TRUE)
    # kmcluster[[k]] <- kmclust
    
    kmclustdf <- data.frame(ind = names(kmclust), cluster = kmclust)
    kmcluster <- merge(kmcluster, kmclustdf, by = "ind")
    colnames(kmcluster)[ncol(kmcluster)] <- paste0("cluster", k)
    clustnum <- as.numeric(substring(as.character(kmcluster[, ncol(kmcluster)]), 2))
    kmcluster <- data.frame(kmcluster,
                            abs(clustnum - as.numeric(as.character(kmcluster$class))))
    colnames(kmcluster)[ncol(kmcluster)] <- paste0("dist", k)
    
    # "confusion matrix" (but the levels of class and clust do not match)
    CM[[k]] <- table(kmcluster[, c("class", paste0("cluster", k))], useNA = "ifany")
    # CM[[k]] <- table(clust[, c("class", "clustmaj")])
    
    colnames(clust)[-1] <- paste(colnames(clust)[-1], k, sep = ".")
    # add the results to the global table clustdists
    clustdist <- merge(clustdist, clust, by = "class")
  }
  
  # # somme des matrices de confusion
  # sumCM <- matrix(nrow = 7, ncol = 7, dimnames = dimnames(CM[[1]]))
  # for (i in 1:nrow(sumCM)) for (j in 1:ncol(sumCM)) {
  #   sumCM[i, j] <- sum(sapply(CM, "[", i, j))
  # }
  # sumCM <- as.table(sumCM)
  
  # mean purity per class
  purmean <- apply(clustdist[seq(3, ncol(clustdist), by = 3)], 1, mean)
  # mean distance per class
  distmean <- apply(clustdist[seq(4, ncol(clustdist), by = 3)], 1, mean)
  
  clust$class <- paste0("c", levels(clust$class))
  
  return(list(clustdist = clustdist,
              purdist.mean = data.frame(class = rownames(clustdist), purity = purmean, distance  = distmean),
              # confusion.matrix = sumCM)),
              classification = kmcluster,
              confusion.matrix = CM))
}
